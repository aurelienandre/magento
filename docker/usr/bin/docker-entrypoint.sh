#!/bin/bash
set -e

if [ -z "$(ls -A "app/etc/env.php" 2> /dev/null)" ]; then
    cp "app/etc/env.php.sample" "app/etc/env.php"
fi

if [ -z "$(ls -A "package.json" 2> /dev/null)" ]; then
    cp "package.json.sample" "package.json"
fi

if [ -z "$(ls -A "grunt-config.json" 2> /dev/null)" ]; then
    cp "grunt-config.json.sample" "grunt-config.json"
fi

if [ -z "$(ls -A "Gruntfile.js" 2> /dev/null)" ]; then
    cp "Gruntfile.js.sample" "Gruntfile.js"
fi

if [ -z "$(ls -A "vendor/autoload.php" 2> /dev/null)" ]; then
    composer install --no-progress --no-suggest --no-interaction
fi

if [ -z "$(ls -A "node_modules" 2> /dev/null)" ]; then
    npm install --ignore-scripts
fi

setfacl -R \
        -m u:www-data:rwX \
        -m u:"${UID}":rwX \
        .
setfacl -dR \
        -m u:www-data:rwX \
        -m u:"${UID}":rwX \
        .

if [ "${1#-}" != "$1" ]; then
	set -- apache2-foreground.sh "$@"
fi

exec "$@"
