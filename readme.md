# Docker Magento v 2.4

## Install

Open hosts
```
sudo nano /etc/hosts
```

Copy rules
```
127.0.0.1       www.traefik.lan
127.0.0.1       www.phpmyadmin.lan
127.0.0.1       www.magento.lan
127.0.0.1       www.redisinsight.lan
127.0.0.1       www.kibana.lan
127.0.0.1       www.elasticsearch.lan
```

Open phpmyadmin and import "data/mysql/dump/install.sql" in "magento" database

## Usages

Show make commands
```
make
```

```
build                Build registry image
push                 Push registry image
up                   Launch all the container
down                 Stop all the container
composer-update      Update composer dependencies
composer-install     Install composer dependencies
mage-cache-clean     Magento cache clean
mage-cache-flush     Magento cache flush
mage-config-import   Magento config import
mage-setup-upgrade   Magento setup upgrade
mage-setup-di        Magento setup di compile
redis-cache-flush    Redis cache flush
redis-session-flush  Redis session flush
redis-flush          Redis flush all
```
