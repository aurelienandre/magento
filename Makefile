.PHONY: help build push up down composer-update composer-install mage-cache-clean mage-cache-flush mage-config-import mage-setup-upgrade mage-setup-di redis-cache-flush redis-session-flush redis-flush
.DEFAULT_GOAL= help

SHELL = /bin/sh

UID := $(shell id -u)
GID := $(shell id -g)

export UID
export GID

DOCKER_REGISTRY_REPOSITORY ?= aurelienandre/magento
DOCKER_REGISTRY_TAG        ?= latest

COMPOSE_MAGENTO             = docker-compose exec magento
COMPOSE_MAGENTO_CONSOLE     = docker-compose exec magento bin/magento

COMPOSE_REDIS_CACHE         = docker-compose exec redis-cache
COMPOSE_REDIS_SESSION       = docker-compose exec redis-session

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-20s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

build: ## Build registry image
	docker build . -t $(DOCKER_REGISTRY_REPOSITORY):$(DOCKER_REGISTRY_TAG)

push: ## Push registry image
	docker push $(DOCKER_REGISTRY_REPOSITORY):$(DOCKER_REGISTRY_TAG)

up: ## Launch all containers
	docker-compose up -d

down: ## Stop all containers
	docker-compose down

composer-update: ## Update composer dependencies
	docker-compose exec magento composer update

composer-install: ## Install composer dependencies
	$(COMPOSE_MAGENTO) composer install --no-suggest --no-progress

mage-cache-clean: ## Magento cache clean
	$(COMPOSE_MAGENTO_CONSOLE) cache:clean

mage-cache-flush: ## Magento cache flush
	$(COMPOSE_MAGENTO_CONSOLE) cache:flush

mage-config-import: ## Magento config import
	$(COMPOSE_MAGENTO_CONSOLE) app:config:import

mage-setup-upgrade: ## Magento setup upgrade
	$(COMPOSE_MAGENTO_CONSOLE) setup:upgrade

mage-setup-di: ## Magento setup di compile
	$(COMPOSE_MAGENTO_CONSOLE) setup:di:compile

redis-cache-flush: ## Redis cache flush
	$(COMPOSE_REDIS_CACHE) redis-cli flushall

redis-session-flush: ## Redis session flush
	$(COMPOSE_REDIS_SESSION) redis-cli flushall

redis-flush: redis-cache-flush redis-session-flush ## Redis flush all
